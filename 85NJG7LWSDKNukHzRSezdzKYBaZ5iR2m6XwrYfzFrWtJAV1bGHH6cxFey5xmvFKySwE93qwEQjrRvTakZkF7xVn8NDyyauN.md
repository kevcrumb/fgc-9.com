---
layout: default.liquid
title: Tip Monero
---

{% comment %}
*(via PrusaPrinters.org)*
{% endcomment %}

If you particularily liked any of my designs like the weighted baseplate or the Screw 'Em Kit you can tip Monero to:

`85NJG7LWSDKNukHzRSezdzKYBaZ5iR2m6XwrYfzFrWtJAV1bGHH6cxFey5xmvFKySwE93qwEQjrRvTakZkF7xVn8NDyyauN`

![monero:85NJG7LWSDKNukHzRSezdzKYBaZ5iR2m6XwrYfzFrWtJAV1bGHH6cxFey5xmvFKySwE93qwEQjrRvTakZkF7xVn8NDyyauN?tx_description=tip&tx_description=kevcrumb](images/qr_prusaprinters.svg "XMR address as QR Code")

<br/>

*Want to pass checkpoints like innocent?* <br/>
*Use [Split Linux](https://splitlinux.org).*


{% for post in collections.posts.pages %}
#### {{post.title}}

[{{ post.title }}]({{ post.permalink }})
{% endfor %}
