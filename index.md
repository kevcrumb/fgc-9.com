---
layout: default.liquid
title: Shortcuts to Freedom
---

Just type `<shortcut>.fgc-9.com`

| Shortcut                     | Target      |
| ---------------------------- | ----------- |
| **dd**                       | `printgunbnujxetu.onion` |
| **chat**                     | `chat.deterrencedispensed.com` |
| **ecm** <br/>or **barrel**   | `odysee.com/@Ivan's_CAD_Streams:c/ECM-DIY-Barrels:a` |
| **screws**                   | `www.prusaprinters.org/prints/74560-fgc-9-screw-em-kit-mkii` |
| **slice**                    | `gitlab.com/kevcrumb/PrusaSlicer` |
| **specs**                    | `ctrlpew.com/fgc9-file-drop-2` |
| **fcg** or <br/> **trigger** | `odysee.com/@TheGatalog-Accessories:e/CSGCG:8` |


<br/>

*Want to pass checkpoints like innocent?* <br/>
*Use [Split Linux](https://splitlinux.org).*


{% for post in collections.posts.pages %}
#### {{post.title}}

[{{ post.title }}]({{ post.permalink }})
{% endfor %}

{% comment %}
![Powered by Split Linux][logo]
![The new FGC-9 carbine][ad]
![I am extremely peaceful][peaceful]
{% endcomment %}

[peaceful]: images/i-am-extremely-peaceful.jpg
[ad]: images/fgc-9_ad.jpg
[logo]: images/logo.png
